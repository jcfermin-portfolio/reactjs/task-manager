# Task Manager Application

A responsive and feature-rich task manager application built with React, MUI, and SCSS. This application allows users to add, edit, complete, and delete tasks. The application follows best practices for modern front-end development, including input validation, responsive design, and clean, maintainable code.

## Features

- **Add Tasks**: Users can add new tasks with a title and description.
- **Complete Tasks**: Mark tasks as completed.
- **Delete Tasks**: Remove tasks from the list.
- **Responsive Design**: Fully responsive design that works on all screen sizes.
- **Input Validation**: Ensures that tasks have both a title and a description before they can be added.
- **Error Handling**: Displays error messages when validation fails.
- **Clean UI**: Modern and clean user interface with consistent styling.

## Technologies Used

- **React**: JavaScript library for building user interfaces.
- **MUI (Material-UI)**: React components for faster and easier web development.
- **SCSS**: CSS preprocessor for cleaner and more maintainable styles.

## Getting Started

### Prerequisites

- Node.js and npm installed on your machine.

### Installation

1. Clone the repository:
   ```bash
   git clone https://github.com/your-username/task-manager.git
   cd task-manager
   npm install
   npm start


### Project Structure
task-manager/
├── public/
├── src/
│   ├── assets/         # Images, fonts, etc.
│   ├── components/     # Reusable components
│   │   └── TaskItem/
│   │       └── TaskItem.tsx
│   │       └── TaskItem.scss
│   ├── hooks/          # Custom hooks
│   ├── interfaces/     # TypeScript interfaces
│   │   └── Task.ts
│   ├── pages/          # Page components
│   │   └── Home/
│   │       └── Home.tsx
│   │       └── Home.scss
│   ├── services/       # API calls and business logic
│   ├── styles/         # Global styles, SCSS files
│   │   ├── _variables.scss
│   │   ├── _mixins.scss
│   │   ├── _base.scss
│   │   ├── _components.scss
│   │   ├── _layout.scss
│   │   └── main.scss
│   ├── utils/          # Utility functions
│   ├── App.tsx
│   ├── index.tsx
│   └── ...
└── tsconfig.json

### SCSS Best Practices
Variables: Defined in _variables.scss for colors, spacing, and font sizes.
Mixins: Common styles defined in _mixins.scss for reuse.
Components: Scoped styles for individual components in their respective SCSS files.
Global Styles: Base styles and global settings in _base.scss, _components.scss, and _layout.scss.