import React from 'react';
import { Task } from '../../interfaces/Task';
import { Checkbox, IconButton, Card, CardContent, Typography, CardActions } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import './TaskItem.scss';

interface TaskItemProps {
  task: Task;
  onToggleComplete: (id: string) => void;
  onDelete: (id: string) => void;
}

const TaskItem: React.FC<TaskItemProps> = ({ task, onToggleComplete, onDelete }) => {
  return (
    <Card className={`task-item ${task.completed ? 'task-item--completed' : ''}`}>
      <CardContent className="task-item__content">
        <Checkbox checked={task.completed} onChange={() => onToggleComplete(task.id)} />
        <div className="task-item__details">
          <Typography variant="h6" component="h3" className="task-item__title">
            {task.title}
          </Typography>
          <Typography variant="body2" className="task-item__description">
            {task.description}
          </Typography>
        </div>
      </CardContent>
      <CardActions>
        <IconButton onClick={() => onDelete(task.id)} color="secondary">
          <DeleteIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default TaskItem;
