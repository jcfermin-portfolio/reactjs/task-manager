import React, { useState } from 'react';
import { Task } from '../../interfaces/Task';
import TaskItem from '../../components/TaskItem/TaskItem';
import { v4 as uuidv4 } from 'uuid';
import { Button, TextField, Container, Grid, Box, Alert, FormControl, FormHelperText } from '@mui/material';
import './Home.scss';

const Home: React.FC = () => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [newTask, setNewTask] = useState<{ title: string; description: string }>({ title: '', description: '' });
  const [error, setError] = useState<string | null>(null);

  const handleAddTask = () => {
    if (!newTask.title || !newTask.description) {
      setError('Both title and description are required.');
      return;
    }
    setError(null);
    setTasks([...tasks, { id: uuidv4(), title: newTask.title, description: newTask.description, completed: false }]);
    setNewTask({ title: '', description: '' });
  };

  const handleToggleComplete = (id: string) => {
    setTasks(tasks.map(task => (task.id === id ? { ...task, completed: !task.completed } : task)));
  };

  const handleDelete = (id: string) => {
    setTasks(tasks.filter(task => task.id !== id));
  };

  return (
    <Container className="home">
      <h1>Task Manager</h1>
      <Box className="home__new-task">
        <Grid container spacing={2} alignItems="flex-start">
          <Grid item xs={12} sm={5}>
            <FormControl fullWidth error={Boolean(error && !newTask.title)}>
              <TextField
                label="Title"
                value={newTask.title}
                onChange={e => setNewTask({ ...newTask, title: e.target.value })}
              />
              {error && !newTask.title && <FormHelperText>Title is required</FormHelperText>}
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={5}>
            <FormControl fullWidth error={Boolean(error && !newTask.description)}>
              <TextField
                label="Description"
                value={newTask.description}
                onChange={e => setNewTask({ ...newTask, description: e.target.value })}
              />
              {error && !newTask.description && <FormHelperText>Description is required</FormHelperText>}
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={2}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              onClick={handleAddTask}
              className="home__add-button"
            >
              Add Task
            </Button>
          </Grid>
        </Grid>
      </Box>
      {error && <Alert severity="error" className="home__error">{error}</Alert>}
      <Grid container spacing={2} className="home__task-list">
        {tasks.map(task => (
          <Grid item xs={12} key={task.id}>
            <TaskItem task={task} onToggleComplete={handleToggleComplete} onDelete={handleDelete} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Home;
